package com.example.tvcharacters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.cell_character.view.*

class CharactersAdapter : RecyclerView.Adapter<CharactersAdapter.CharacterViewHolder> (){

    private val characterList = arrayOf(Character("Richard Hendricks","Silicon Valley"),
                                Character("Jared Dunn", "Silicon Valley"),
                                Character("Will McAvow","The News Room"),
                                Character("Mackenzie MacHale", "The News Room"),
                                Character("Jim Harper","The News Room"),
                                Character("Ross Geller", "Friends"),
                                Character("Chandler Bing","Friends"),
                                Character("Joey tribal", "Friends"),
                                Character("Monica Geller", "Friends"),
                                Character("Charlie Harper", "Two and a Half Men"),
                                Character("Allan Harper", "Two and a Half Men"),
                                Character("Jake Harper", "Two and a Half Men"))

    inner class CharacterViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        val ui_title = itemView.ui_title
        val ui_substitle = itemView.ui_substitle

        // gerer les clics sur la cellule entière
        init {
            itemView.setOnClickListener(this)
        }

        fun fillWithCharacter(character: Character) {
            ui_title.text = character.name
            ui_substitle.text = character.show

        }

        override fun onClick(p0: View?) {
            if (p0 != null) {
                onCharacterClick(adapterPosition, p0.context)
            }
        }

    }

    private fun onCharacterClick(index: Int, context: Context?) {
            val character = characterList[index]
        Toast.makeText(context, character.name, Toast.LENGTH_SHORT).show()
    }

    override fun getItemCount(): Int {

        return characterList.size
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CharacterViewHolder {
        
         // 1. charger la vue xml
        val rootView = LayoutInflater.from(parent.context).inflate(R.layout.cell_character, parent,false)

        // 2. créer un viewHolder pour contrôler cette vue
        val holder = CharacterViewHolder(rootView)

        // 3. retourner le viewHolder
        return  holder
    }

    override fun onBindViewHolder(holder: CharacterViewHolder, position: Int) {

        // 1. obtenir le personnage
        val character = characterList[position]

        // 2. envoyer les infos du personnage dans le holder
        holder.fillWithCharacter(character)
    }


}